package com.rxtxdemo.rxtxdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RxtxdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RxtxdemoApplication.class, args);
    }
}
