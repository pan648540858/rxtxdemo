package com.rxtxdemo.rxtxdemo.task;

import com.rxtxdemo.rxtxdemo.core.SerialPortCore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/*
 *
 * @author  潘林林
 * @date  2018/5/28 11:33
 *
 */
@Component
public class SerialPortTask implements CommandLineRunner {

    @Value("${serialPortName}")
    private String serialPortName;

    @Autowired
    private SerialPortCore serialPortCore;

    @Override
    public void run(String... args) throws Exception {
        // 连接
        serialPortCore.connect(serialPortName);

        Thread.sleep(2000);
        // 发送消息
        serialPortCore.sendMsg("test");
    }
}
