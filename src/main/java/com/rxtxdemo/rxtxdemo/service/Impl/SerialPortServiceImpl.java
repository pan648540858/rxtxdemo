package com.rxtxdemo.rxtxdemo.service.Impl;

import com.rxtxdemo.rxtxdemo.service.ISerialPortService;
import org.springframework.stereotype.Service;

/*
 *
 * @author  潘林林
 * @date  2018/5/28 14:52
 *
 */
@Service
public class SerialPortServiceImpl implements ISerialPortService {

    @Override
    public void serialPortMsgHandler(String msg) {
        System.out.println("收到端口消息： " + msg);
    }
}
